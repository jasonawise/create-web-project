import os

import settings

class Project():
    def __init__(self):
        self.name = ''

    def get_project_name(self):
        self.name = input('What would you like to call your new project?: ')

    def display_current_directory(self):
        print(os.getcwd())

    def change_to_project_directory(self):
        os.chdir(settings.project_directory)

    def create_sub_folders(self):
        os.mkdir(settings.css)
        os.chdir(settings.css)
        self.create_css_file()

        os.chdir('..')


        os.mkdir(settings.javascript)
        os.chdir(settings.javascript)
        self.create_javascript_file()

        os.chdir('..')
        os.mkdir(settings.assets)

    def create_html_file(self):
        open('index.html', 'w+')

    def create_css_file(self):
        open('custom.css', 'w+')

    def create_javascript_file(self):
        open('main.js', 'w+')

    def create_project_structure(self):
        self.change_to_project_directory()
        project_name = self.name

        os.mkdir(project_name)
        os.chdir(project_name)
        self.create_html_file()
        self.create_sub_folders()


my_project = Project()
my_project.get_project_name()
my_project.create_project_structure()
my_project.display_current_directory()
