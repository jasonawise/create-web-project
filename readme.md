You will need to create a settings.py file and in it you will need to create the necessary variables to be used in main.py
You will need to define a project_directory and names for the folders.

This works really well if you setup a bash/zsh/shell alias to run the script.
Here is what I am using:
alias createwebproject="python3 ~/PATH TO PROJECT/main.py"